﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using WebApiTest.DB;
using WebApiTest.Models;
using WebApiTest.Services;

namespace WebApiTest.Controllers
{
    [RoutePrefix("api/ipstack")]
    public class IpStackController : ApiController
    {
        private IDbMongoRepository _dbRepository;
        private IApiService _apiService;

        public IpStackController()
        {
            var dbContext = new DbMongoContext();
            _dbRepository = new DbMongoRepository(dbContext);
            _apiService = new ApiService(_dbRepository);
        }

        [HttpGet]
        public async Task<IEnumerable<Address>> Get()
        {
            return await _dbRepository.GetAllAddresses();
        }

        [HttpGet]
        [Route("getlocationdata")]
        public async Task<IHttpActionResult> GetLocationData(string ipaddress)
        {
            var address = await _apiService.GetLocationData(ipaddress);

            return Ok(address);
        }

        [HttpPost]
        //[Route("getlocationdata")]
        public async Task<IHttpActionResult> Post([FromBody] string ipaddress)
        {
            var address = await _apiService.GetLocationData(ipaddress);

            return Created(address.ip, address);
        }
    }
}
