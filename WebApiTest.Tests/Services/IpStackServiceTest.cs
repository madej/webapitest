﻿using System;
using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using WebApiTest.DB;
using WebApiTest.Models;
using WebApiTest.Services;

namespace WebApiTest.Tests.Services
{
    [TestFixture]
    public class IpStackServiceTest
    {
        [Test]
        public void ServiceTestMethod1()
        {
            var repository = new Mock<IDbMongoRepository>();

            var addressList = new List<Address>()
            {
                new Address() {ip = "0.0.0.0" },
                new Address() {ip = "1.1.1.1" }
            };

            repository.Setup(s => s.GetAllAddresses()).ReturnsAsync(addressList);

            var apiService = new ApiService(repository.Object);
            var data = apiService.GetAllAddresses();

            NUnit.Framework.Assert.AreEqual(2, (data.Result as List<Address>).Count);

        }
    }
}
