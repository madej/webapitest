﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using WebApiTest.Models;

namespace WebApiTest.DB
{
    public class DbMongoRepository : IDbMongoRepository
    {
        private readonly IDbMongoContext _dbContext;

        public DbMongoRepository(IDbMongoContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<Address>> GetAllAddresses()
        {
            return await _dbContext.Addresses.Find(new BsonDocument()).ToListAsync();
        }

        public async Task Insert(Address address)
        {
            await _dbContext.Addresses.InsertOneAsync(address);
        }
    }
}