﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiTest.Models
{
    [BsonDiscriminator("Address")]
    public class Address
    {
        [BsonId]
        public ObjectId id { get; set; }
        public string ip { get; set; }
        public string type { get; set; }
        public string continent_code { get; set; }
        public string continent_name { get; set; }
        public string country_code { get; set; }
        public string country_name { get; set; }
        public string region_code { get; set; }
        public string region_name { get; set; }
        public string city { get; set; }
        public string zip { get; set; }
        public string latitude { get; set; }
        public location location { get; set; }
    }

    public class location
    {
        public string geoname_id { get; set; }
        public string capital { get; set; }
        public List<languages> languages { get; set; }
        public string country_flag { get; set; }
        public string country_flag_emoji { get; set; }
        public string country_flag_emoji_unicode { get; set; }
        public string calling_code { get; set; }
        public bool is_eu { get; set; }
    }

    public class languages
    {
        public string code { get; set; }
        public string name { get; set; }
        public string native { get; set; }
    }
}