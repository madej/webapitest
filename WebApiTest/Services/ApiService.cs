﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using WebApiTest.DB;
using WebApiTest.Models;

namespace WebApiTest.Services
{
    public class ApiService: IApiService
    {
        private IDbMongoRepository _dbRepository;
        public ApiService(IDbMongoRepository dbRepository)
        {
            _dbRepository = dbRepository;
        }

        public async Task<IEnumerable<Address>> GetAllAddresses()
        {
            return await _dbRepository.GetAllAddresses();
        }

        public async Task<Address> GetLocationData(string ipAddress)
        {
            var key = ConfigurationManager.AppSettings["ipStackKey"];
            Address address = null;

            HttpClient client = new HttpClient();
            var ipStack = $"http://api.ipstack.com/{ipAddress}?access_key={key}";
            
            HttpResponseMessage response = await client.GetAsync(ipStack);
            if (response.IsSuccessStatusCode)
            {
                address = await response.Content.ReadAsAsync<Address>();
                await _dbRepository.Insert(address);
            }
            return address;
        }
    }
}