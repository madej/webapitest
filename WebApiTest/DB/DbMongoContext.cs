﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using WebApiTest.Models;

namespace WebApiTest.DB
{
    public class DbMongoContext: IDbMongoContext
    {
        private readonly IMongoDatabase _db;

        public DbMongoContext()
        {
            var databaseName = "ApiTest";
            var connectionString = ConfigurationManager.AppSettings["dbConnection"];
            var client = new MongoClient(connectionString);

            _db = client.GetDatabase(databaseName);
            //IMongoCollection<BsonDocument> collection;
        }

        public IMongoCollection<Address> Addresses => _db.GetCollection<Address>("Address");
    }
}