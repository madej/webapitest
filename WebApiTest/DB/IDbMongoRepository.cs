﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApiTest.Models;

namespace WebApiTest.DB
{
    public interface IDbMongoRepository
    {
        Task<IEnumerable<Address>> GetAllAddresses();
        Task Insert(Address address);
    }
}
