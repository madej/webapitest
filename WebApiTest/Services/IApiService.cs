﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApiTest.Models;

namespace WebApiTest.Services
{
    public interface IApiService
    {
        Task<IEnumerable<Address>> GetAllAddresses();
        Task<Address> GetLocationData(string ipAddress);
    }
}
