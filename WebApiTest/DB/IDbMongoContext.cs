﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApiTest.Models;

namespace WebApiTest.DB
{
    public interface IDbMongoContext
    {
        IMongoCollection<Address> Addresses { get; }
    }
}
